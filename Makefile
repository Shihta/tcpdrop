CC      = /usr/gnu/bin/cc
LD      = /usr/gnu/bin/ld
TMPDIR  = ./build
DESTDIR = $(TMPDIR)

all: tcpdrop

install: all
	mkdir -p $(DESTDIR)/usr/bin
	cp tcpdrop $(DESTDIR)/usr/bin

tcpdrop: tcpdrop.o
	$(CC) -o tcpdrop -lsocket -lnsl tcpdrop.o

clean:
	rm -f tcpdrop tcpdrop.o
	rm -rf $(TMPDIR)
	rm tcpdrop.p5m*
